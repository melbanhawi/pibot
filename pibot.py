#! /usr/bin/python3
import gpiozero
import time

class Motor(object):
    
    def __init__(self, forward_pin: int, backward_pin: int, speed_pin: int):
        self.back_output = gpiozero.OutputDevice(backward_pin) # digital output
        self.front_output = gpiozero.OutputDevice(forward_pin) # digital output
        self.pwm = gpiozero.PWMOutputDevice(speed_pin) # set up PWM pin
    
    def stop(self):
        self.back_output.off()
        self.front_output.off()
        
    def move_forward(self, speed: float):
        self.pwm.value = speed
        self.back_output.off()
        self.front_output.on()
        
    def move_backward(self, speed: float):
        self.pwm.value = speed
        self.back_output.on()
        self.front_output.off()
        
    def brake(self):
        self.move_forward(speed = 0)
        
        
class DiffDrive(object):

    def __init__(self, left_motor: Motor, right_motor: Motor):
        self.left_motor = left_motor
        self.right_motor = right_motor
        self.speed = 0.6
    
    def set_speed(self, speed: float = 0.6):
        self.speed = speed
        
    def turn_right(self):
        self.right_motor.move_forward(self.speed)
        self.left_motor.brake()
    
    def turn_left(self):
        self.right_motor.brake()
        self.left_motor.move_forward(self.speed)
        
    def move_forward(self):
        self.right_motor.move_forward(self.speed)
        self.left_motor.move_forward(self.speed)
        
    def move_backward(self):
        self.right_motor.move_backward(self.speed)
        self.left_motor.move_backward(self.speed)
    
    def brake(self):
        self.right_motor.brake()
        self.left_motor.brake()
        
    def stop(self):
        self.right_motor.stop()
        self.left_motor.stop()
    

if __name__ == "__main__":

    if 0:
        # test sensors
        sensor = gpiozero.DistanceSensor(trigger = 27, echo = 22, max_distance = 0.45)

        while True:
            time.sleep(0.5)
            print(sensor.distance)

    # test motors
    if 0:
        right = Motor(15,14,12)
        left = Motor(5,6,13)
        pibot = DiffDrive(left, right)
        pibot.brake()
        time.sleep(1)
    
        print("turning left")
        pibot.turn_left()
        time.sleep(1)
        pibot.brake()
        time.sleep(1)
    
        print("turning right")
        pibot.turn_right()
        time.sleep(1)
        pibot.brake()
        time.sleep(1)
    
        print("moving forward")
        pibot.move_forward()
        time.sleep(1)
        pibot.brake()
        time.sleep(1)
    
        print("moving backward")
        pibot.move_backward()
        time.sleep(1)
        pibot.brake()
